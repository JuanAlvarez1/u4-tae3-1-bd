#Enunciado del Problema
'''Escriba un programa para construir un Sistema de Gestión de Estudiantes simple usando Python que pueda realizar
las siguientes operaciones sobre la base de datos "academia" construida en las clases de la asignatura:
1-Matricular estudiante
2-Visualizar datos del estudiante
3-Buscar estudiante por id
4-Eliminar estudiante por id'''
'''Colocar el enlace a su repositorio gitlab públic en donde cargue el código, imágenes de la ejecución, y
 otros archivos que considere importantes dentro de su programa.'''
import sqlite3

class estudiantesbd:
    nom = ''
    email = ''
    carrera = ''
    ciclo = int
    ci = int
    id = int
    def __init__(self):
        self.conexion = sqlite3.connect('academia.sqlite')
        self.cursor = None
        print("Conexion establecida !!!")

    def crear_bd(self):
        sql_academia = """CREATE TABLE Academia (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, ci INTEGER, 
        nombre   TEXT UNIQUE, 
        email  TEXT, carrera TEXTO, ciclo INTEGER)"""

        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql_academia)
        print("Creadas las tablas !!!")
        self.cursor.close()


    def Matric_est(self,ci,nom, email, carrera, ciclo):
        self.ci = ci
        self.nom = nom
        self.email = email
        self.carrera = carrera
        self.ciclo = ciclo
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO Academia(ci, nombre, email, carrera, ciclo) VALUES(?,?,?,?,?)",
                                [(self.ci,self.nom,self.email,self.carrera,self.ciclo)])
        self.conexion.commit()
        self.cursor.close()
        print ("Usuarios matriculado")

    def visualizar_datos(self,nom):
        self.nom = nom
        sql = "SELECT * FROM Academia  WHERE ci =" + self.nom
        self.cursor = self.conexion.cursor()

        filas = self.cursor.execute(sql)
        for f in filas:
            print(f)
        self.cursor.close()

    def buscar_datos(self,id):
        self.id = id
        sql = "SELECT * FROM Academia  WHERE id =" + self.id
        self.cursor = self.conexion.cursor()
        filas = self.cursor.execute(sql)
        for f in filas:
            print(f[2])
        self.cursor.close()

    def eliminar_datos(self,id):
        self.id = id
        sql = "DELETE FROM Academia  WHERE id =" + self.id
        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql)
        self.conexion.commit()
        self.cursor.close()


if __name__ == "__main__":
    base = estudiantesbd()
    #base.crear_bd()
    print ('\t\t\tSGE \n'
           'Operaciones disponibles en la base de datos Academia:\n'
           '1.Matricular estudiante.\n'
           '2.Visualizar datos del Estudiante\n'
           '3.Buscar estudinate por ID\n'
           '4.Eliminar estudiante por ID\n')
    while True:
        op = int(input('¿Qué operación quieres realizar? (1,2,3,4) \n'))
        if op == 1:
            ci = int(input ('Para Matricularte ingresa los siguientes datos\nIngresa numero de cedula: '))
            nom = input ('Ingrese sus nombres completos: ')
            email = input ('Ingresa tu dirección de correo electronico:')
            carrera = input ('Ingresa la carrera en la que deseas matricularte:')
            ciclo = int(input('Ingresa el ciclo al que deseas matricularte:'))
            base.Matric_est(ci, nom, email, carrera, ciclo)
        if op == 2:
            ci = input('Ingrese el numero de C.I. a consultar: ')
            base.visualizar_datos(ci)
        if op == 3:
            id = input('Ingrese un numero de id a buscar: ')
            base.buscar_datos(id)
        if op == 4:
            id = input('Ingrese un numero de id a eliminar: ')
            base.eliminar_datos(id)
        else:
            """print ('ingrese una opción correcta')"""

